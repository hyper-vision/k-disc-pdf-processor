import fitz
import os
import shutil
import face_recognition
from joblib import dump, load
from django.conf import settings
from django.db import models

# TODO: Make it write image to media folder
 
def extract_image(document):
    doc = fitz.open(document)
    for i in range(len(doc)):
        # for img in doc.getPageImageList(i):
        img = doc.getPageImageList(i)[0]
        xref = img[0]
        pix = fitz.Pixmap(doc, xref)
        if pix.n < 5:       # this is GRAY or RGB
            pix.writePNG("face_%s.png" % (i))
        else:               # CMYK: convert to RGB first
            pix1 = fitz.Pixmap(fitz.csRGB, pix)
            pix1.writePNG("face_%s.png" % (i))
            pix1 = None
        pix = None
        # Move from base directory to 'photo' directory
        shutil.copy(os.path.join(settings.BASE_DIR, "face_{}.png".format(i)), settings.MEDIA_ROOT)
        # Remove copy in base-dir
        os.remove(os.path.join(settings.BASE_DIR, "face_{}.png".format(i)))

        # TODO: Remove PDF files from media-root (TEMP SOLUTION)

        for file in os.listdir(settings.MEDIA_ROOT):
            if file.endswith(".pdf"):
                try:
                    os.remove(os.path.join(settings.MEDIA_ROOT, file))
                except:
                    continue
    

        return os.path.join(settings.MEDIA_ROOT, "face_{}.png".format(i)),  "face_{}.png".format(i)

ALLOWED_EXTENSIONS = {'jpg', 'png', 'jpeg'}

def predict(test_img_path, custom_model = None, model_path = None, distance_threshold = 0.4):
    if not os.path.isfile(test_img_path):
        raise Exception("Invalid image path: {}".format(test_img_path))
    # Splits like so (<file path>, <extension>)
    if os.path.splitext(test_img_path)[1][1:] not in ALLOWED_EXTENSIONS:
        raise Exception("Invalid file format: {}".format(os.path.splitext(test_img_path)[1][1:]))
    if custom_model is None and model_path is None:
        raise Exception("Model required!")

    if custom_model is None:
        with open(model_path, 'rb') as f:
            clf = load(f)


    img = face_recognition.load_image_file(test_img_path)
    face_locations = face_recognition.face_locations(img)

    if len(face_locations) == 0:
        return []
    
    face_encodings = face_recognition.face_encodings(img, face_locations)

    closest_distance = clf.kneighbors(face_encodings, n_neighbors = 1)
    # print(closest_distance)
    matches = [closest_distance[0][i][0] <= distance_threshold for i in range(len(face_locations))]

    # print(clf.predict(face_encodings))

    # Return the name and probability, get code from colab
    return [(prediction, probability) if recognized else ('Unknown', probability) for prediction, probability, recognized in zip(clf.predict(face_encodings), clf.predict_proba(face_encodings), matches)]
