import re
import os
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.shortcuts import render, redirect
from django.contrib import messages
from django.http import HttpResponse
from .processor import extract_image, predict

# Create your views here.
def index(request):
    if request.method == "POST":
        if not os.path.isdir(settings.MEDIA_ROOT):
            os.mkdir(settings.MEDIA_ROOT)

        uploaded_file = request.FILES["document"]
        extension = re.search(r"[^\/]+$", uploaded_file.content_type)[0]
        if extension != "pdf":
            messages.warning(request, f"Invalid filetype '.{extension}'! Please upload a PDF")
            return redirect('app-home')
        else:
            # Save the uploaded file
            fs = FileSystemStorage()
            fileName = fs.save(uploaded_file.name, uploaded_file)
            # Extracts the image to a folder called 'photo' in Base Directory and returns the path to it
            img_path, img_name = extract_image(os.path.join(settings.MEDIA_ROOT, fileName)) 

            print(img_path)
            # Predicts the image in the folder 'photo'
            results = predict(test_img_path = img_path, model_path=os.path.join(settings.BASE_DIR, "trained_knn_model.clf"))
            context = dict()

            if results[0] == "Unknown":
                prob = (1 - results[0][1].max() * 100)
            else:
                prob = (results[0][1].max() * 100)

            # Add to context variable
            print(img_path)
            context['Name'] = results[0][0]
            context['Probability'] = prob
            context['ImgAddress'] = "/media/{}".format(img_name)

            return render(request, "MainApp/output.html", context = context)

    return render(request, "MainApp/index.html")

def help(request):
    return render(request, "MainApp/help.html")
