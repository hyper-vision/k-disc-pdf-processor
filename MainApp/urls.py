from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name = "app-home"),
    path("help/", views.help, name = "app-help")
]

